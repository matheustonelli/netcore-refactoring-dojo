﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        #region 1. Atualizar Qualidade
        public void AtualizarQualidade()
        {
            foreach(Item item in Itens)
            {
                if (item.Nome == "Sulfuras, a Mão de Ragnaros")
                    continue;

                item.PrazoValidade -= 1;

                if (item.Nome == "Queijo Brie Envelhecido")
                {
                    AtualizarQualidadeItemQueijoBrieEnvelhecido(item);
                }
                else if(item.Nome == "Ingressos para o concerto do TAFKAL80ETC")
                {
                    AtualizarQualidadeItemIngressosConcertoTAFKAL80ETC(item);
                }
                else if (item.Nome.Contains("Conjurado"))
                {
                    AtualizarQualidadeItemConjurado(item);
                }
                else
                {
                    AtualizarQualidadeItemComum(item);
                }
            }
        }

        private int DecrementarQualidade(int qualidade, int decremento = 1)
        {
            if ((qualidade - decremento) <= 0)
                return 0;
            
            return qualidade - decremento;
        }

        private int IncrementarQualidade(int qualidade, int incremento = 1)
        {
            if ((qualidade + incremento) >= 50)
                return 50;
            
            return qualidade + incremento;
        }

        private void AtualizarQualidadeItemQueijoBrieEnvelhecido(Item item)
        {
            if (item.PrazoValidade < 0)
            {
                item.Qualidade = IncrementarQualidade(item.Qualidade, 2);
            }
            else
            {
                item.Qualidade = IncrementarQualidade(item.Qualidade);
            }
        }

        private void AtualizarQualidadeItemIngressosConcertoTAFKAL80ETC(Item item)
        {
            if (item.PrazoValidade < 0)
            {
                item.Qualidade = 0;
            }
            else if (item.PrazoValidade < 5)
            {
                item.Qualidade = IncrementarQualidade(item.Qualidade, 3);
            }
            else if (item.PrazoValidade < 10)
            {
                item.Qualidade = IncrementarQualidade(item.Qualidade, 2);
            }
            else
            {
                item.Qualidade = IncrementarQualidade(item.Qualidade, 1);
            }
        }

        private void AtualizarQualidadeItemConjurado(Item item)
        {
            item.Qualidade = DecrementarQualidade(item.Qualidade, 2);
        }

        private void AtualizarQualidadeItemComum(Item item)
        {
            if (item.PrazoValidade < 0)
            {
                item.Qualidade = DecrementarQualidade(item.Qualidade, 2);
            }
            else
            {
                item.Qualidade = DecrementarQualidade(item.Qualidade);
            }
        }
        #endregion
    }
}
