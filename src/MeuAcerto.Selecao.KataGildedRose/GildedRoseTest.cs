﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {

        [Fact]
        public void AtualizarQualidade_ItemQualquer_DecrementarPrazoValidadeEQualidade()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "The One Ring",
                    PrazoValidade = 10,
                    Qualidade = 50
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(49, Items[0].Qualidade);
            Assert.Equal(9, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemQualquer_DecrementarQualidadeEmDois()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "The One Ring",
                    PrazoValidade = 0,
                    Qualidade = 50
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(48, Items[0].Qualidade);
            Assert.Equal(-1, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemQualquer_QualidadeContinuaZero()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "The One Ring",
                    PrazoValidade = 10,
                    Qualidade = 0
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(0, Items[0].Qualidade);
            Assert.Equal(9, Items[0].PrazoValidade);
        }


        [Fact]
        public void AtualizarQualidade_ItemQueijoBrieEnvelhecido_IncrementarQualidade()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Queijo Brie Envelhecido",
                    PrazoValidade = 10,
                    Qualidade = 30
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(31, Items[0].Qualidade);
            Assert.Equal(9, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemQueijoBrieEnvelhecido_AtingirMaximaQualidade()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Queijo Brie Envelhecido",
                    PrazoValidade = 10,
                    Qualidade = 50
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(50, Items[0].Qualidade);
            Assert.Equal(9, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemSulfurasAMaoDeRagnaros_NaoAlterarPrazoValidadeEQualidade()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Sulfuras, a Mão de Ragnaros",
                    PrazoValidade = 10,
                    Qualidade = 80
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(80, Items[0].Qualidade);
            Assert.Equal(10, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemIngressosParaOConcertoDoTAFKAL80ETC_IncrementarQualidade()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 15,
                    Qualidade = 30
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(31, Items[0].Qualidade);
            Assert.Equal(14, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemIngressosParaOConcertoDoTAFKAL80ETC_IncrementarQualidadeEmDois()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 10,
                    Qualidade = 30
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(32, Items[0].Qualidade);
            Assert.Equal(9, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemIngressosParaOConcertoDoTAFKAL80ETC_IncrementarQualidadeEmTres()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 5,
                    Qualidade = 30
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(33, Items[0].Qualidade);
            Assert.Equal(4, Items[0].PrazoValidade);
        }

        [Fact]
        public void AtualizarQualidade_ItemIngressosParaOConcertoDoTAFKAL80ETC_ZerarQualidade()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 0,
                    Qualidade = 30
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(0, Items[0].Qualidade);
            Assert.Equal(-1, Items[0].PrazoValidade);
        }

                [Fact]
        public void AtualizarQualidade_ItemConjurado_DecrementarQualidadeEmDois()
        {
            IList<Item> Items = new List<Item> {
                new Item {
                    Nome = "Bolo de Mana Conjurado",
                    PrazoValidade = 10,
                    Qualidade = 30
                }
            };
            GildedRose app = new GildedRose(Items);

            app.AtualizarQualidade();

            Assert.Equal(28, Items[0].Qualidade);
            Assert.Equal(9, Items[0].PrazoValidade);
        }
    }
}
